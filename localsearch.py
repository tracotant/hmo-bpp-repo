# HEURISTICS FOR MATHEMATICAL OPTIMIZAITON
# BIN PACKING PROBLEM SOLVER
# BY STEFANO CASARIN

import numpy as np
import matplotlib.pyplot as plt

def importInstance(name='default'):
    #import the given instance
    data = np.loadtxt(name,dtype=int)
    n = data[0] #n° of items
    c = data[1] #capacity of the bins
    w = data[2:] #weight of each item
    return (n,c,w)

def fillBin(unpacked,C):
    containers = []         # list of bins
    box = []                # an empty bin
    spaceleft = C       
    while len(unpacked):    # until there are unpacked items
        if len(unpacked) >=ngrasp: # if there are "ngrasp" or more unpacked items
            item = np.random.randint(ngrasp) #pick one at random
        else:
            m = len(unpacked) -1            
            if m > 0:           # if there are 0<n<ngrasp items
                item = np.random.randint(m) # pick one of those random
            else:
                item = 0        # else take the last
        weight = unpacked[item] 
        if weight < spaceleft:  # if it fits
            box.append(weight)  # put it in the box
            spaceleft -= weight # calculate the remaining free space
            unpacked.pop(item)  # remove the item from the unpacked list
        else:                   # if the bin is full
            box.sort()#(reverse=False)   # sort the items in the box
            containers.append(box) # add the bin to the list of full bins
            box = []            # new bin
            spaceleft = C
    containers.append(box)
    return containers

def bpp_grasp(instance,runs):
    (n,c,w) = importInstance(instance)
    w[::-1].sort()
    unpacked = list(w)          # list of items not packed yet
    solutions = []
    for i in range(runs):       # perform several runs
        containers = fillBin(unpacked,c)
        containers.sort()#(reverse=False) # sort containers 
        if not containers in solutions: # if solution already exists pass
            solutions.append(containers)
        unpacked = list(w)
    return solutions

def pickBin(containers,C):
    nbins = len(containers)             #number of bins
    pos = np.random.randint(nbins)      #pick a random element from the list (bin)
    chosen_bin = containers[pos]        #this is the picked bin
    spaceleft = C - sum(chosen_bin)     #how much space left it has  
    while spaceleft == 0:               # do not pick a bin that is already full
        pos = np.random.randint(nbins)
        chosen_bin = containers[pos]
        spaceleft = C - sum(chosen_bin)
    return (pos,chosen_bin)
        
def pickItem(bin_):
    nitems = len(bin_)                  #number of items
    pos = np.random.randint(nitems)     #pick a random element from the list (item)
    return pos                          #return the position and the item

def mergeBins(containers,C):  
    merged_containers = containers.copy()               # create a copy
    
    weights = [sum(el) for el in merged_containers]     # create a list of how much full is the bin
    bin1_ind = weights.index(min(weights))              # pick the emptiest bin
    bin1 = merged_containers[bin1_ind]
    weights.pop(bin1_ind)                               # remove it from the both lists
    merged_containers.pop(bin1_ind)                     # the list of weights and of bins
    bin2_ind = weights.index(min(weights))              # pick the (second) emptiest bin
    bin2 = merged_containers[bin2_ind]                  
    merged_containers.pop(bin2_ind)                     # remove it from bins list
    if (sum(bin1)+sum(bin2)) <= C:                      # if both can fit into one bin
        merged = bin1+bin2                              # merge them
        merged_containers.append(merged)
    else:
        merged_containers.append(bin1)                  # if not put them back to the solution
        merged_containers.append(bin2)
    return merged_containers

def swapItems(containers,C):
    bins = containers               
    (pbin1,bin1) = pickBin(bins,C)  # pick bin 1
    bins.pop(pbin1)                 # remove bin1 from the bins
    (pbin2,bin2) = pickBin(bins,C)  # pick bin 2
    bins.pop(pbin2)                 # remove bin1 from the bins
    pi1 = pickItem(bin1)            # pick item from bin1
    pi2 = pickItem(bin2)            # pick item from bin2
    
    space1 = C - sum(bin1)          # space free in bins 1 and 2
    space2 = C- sum(bin2)
    
    if bin1[pi1] <= space2:         # if item1 can fit in bin2
        bin2.append(bin1.pop(pi1))  # simply take it out and put it there
        bins.append(bin2)           # put bin2 back in the solution
        if bin1 != []:              # if bin1 is not empty
            bins.append(bin1)       # put bin1 in the solution
        return bins
    elif bin2[pi2] <= space1:       # if item2 can fit in bin1
        bin1.append(bin2.pop(pi2))  # do the same as before
        bins.append(bin1)
        if bin2 != []:
            bins.append(bin2)
        return bins
    else:                           # if neither items can fit in the other bin
        bin1[pi1],bin2[pi2] = bin2[pi2],bin1[pi1] # try to swap:
        if (sum(bin1) <= C) & (sum(bin2) <= C): # if there is no violation of the capacities
            bins.append(bin1)       # put the resulting bins in the solution
            bins.append(bin2)
            return bins
        else:                       # if by doing the swap there is a violation
            bin1[pi1],bin2[pi2] = bin2[pi2],bin1[pi1]   #swap back the items to their initial bin
            bins.append(bin1)       # put back the bins in the solution
            bins.append(bin2)
            bins = swapItems(bins, C) # recursively try to swap the bins again
            return bins

def check(bins):
    # function to check if the solution is valid (all the items are packed and
    # there is no capacity violation)
    tot = 0
    tru = []
    for el in bins:
        tot += sum(el)
        tru.append((sum(el)<=c))
    print(tot)
    if all(tru) == 1:
        print('all bins ok')
    else:
        print('at least 1 invalid bin')

def bpp_ls(instance,runs,stop):
    start = bestSol(bpp_grasp(instance, runs))  # take the best solution of GRASP
    plotSolution(start, "Grasp solution")       # plot it for reference
    new = swapItems(start, c)                   # new solutions: try to swap items
    new = mergeBins(new, c)                     # try to merge the containers
    ct = 0 
    while (len(new) == len(start)) | ct < stop: # repeat the operations until the
        new = swapItems(new,c)                  # new solution is better than the previous
        new = mergeBins(new,c)                  # or stop after a number of iterations
        if len(new) < len(start):
            new = start
            ct = 0
        ct +=1
    return new

def bestSol(solutions):
    lensol = [len(solutions[i]) for i in range(len(solutions))] #how many bins in each solution
    bestsol = solutions[np.argmin(lensol)] #the minimum amount of bins
    return bestsol

def plotSolution(containers,title,export=0):
    (n,c,w) = importInstance(instance)
    ncont = len(containers)
    el1 = tuple(containers[i][0] for i in range(ncont))    
    el2 = []
    el3 = []
    for i in range(ncont):
        try:
            el2.append(containers[i][1])
        except IndexError:
            el2.append(0)
        try:
            el3.append(containers[i][2])
        except IndexError:
            el3.append(0)
            
    fig,ax = plt.subplots()
    
    inds = np.arange(1,ncont+1)
    ax.bar(inds,el1,color='royalblue')
    ax.bar(inds,el2,bottom=el1,color='orange')
    ax.bar(inds,el3,bottom=np.array(el1)+np.array(el2),color='forestgreen')
    
    ax.set_title(title)
    ax.set_xticks(np.arange(1,ncont+1,1))
    ax.set_yticks(np.arange(0,c+1,c/5))
    plt.tight_layout()
    if export == 1:
        plt.savefig(title+'.pdf',orientation='landscape')
    plt.show()
    
def printsol(solutions):
    lensol = len(solutions)
    print("{0:^s}".format("BPP -- Grasp + Local Search solver"))
    print("{0:<s} {1:>d}".format("Min. number of bins:",lensol))


instance = 'instances/Falkenauer_t60_01.txt'
(n,c,w) = importInstance(instance)

runs = 10           # runs of the GRASP heuristic
ngrasp = int(len(w)/3)         # first elements from which pick a random item
stop = 1000         # max number of iterations for the local search

sol = bpp_ls(instance,runs,stop) #local search solution
plotSolution(sol,"BPP - Solution grasp + local search.",1)     # plot best solution
printsol(sol)     # print