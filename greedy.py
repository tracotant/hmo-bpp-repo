# HEURISTICS FOR MATHEMATICAL OPTIMIZAITON
# BIN PACKING PROBLEM  - Greedy strategy
# BY STEFANO CASARIN

import numpy as np
import matplotlib.pyplot as plt

def importInstance(name='default'):
    #import the given instance
    data = np.loadtxt(name,dtype=int)
    n = data[0] #n° of items
    c = data[1] #capacity of the bins
    w = data[2:] #weight of each item
    return (n,c,w)

def fillBins(unpacked,C):
    containers = []     #list of bins
    box = []            #an empty bin
    spaceleft = C
    while len(unpacked):            # until there are unpacked items
        weight = unpacked[0]        # pick the first item
        if weight <= spaceleft:      # if it fits
            box.append(weight)      # put it in the box
            spaceleft -= weight     # calculate the remaining free space
            unpacked.pop(0)         # remove the item from the unpacked list
        else:                       # if the bin is full
            containers.append(box)  # add the bin to the list of full bins
            box = []                # take a new box
            spaceleft = C          
    containers.append(box)            
    return containers

def bpp_greedy(instance):
    (n,c,w) = importInstance(instance)
    w[::-1].sort()                        # sort items in ascending order
    unpacked = list(w)              # list of items not packed yet
    containers = fillBins(unpacked,c) #returns list of bins
    return containers

def plotSolution(containers,title,export=0):
    (n,c,w) = importInstance(instance)
    ncont = len(containers)
    el1 = tuple(containers[i][0] for i in range(ncont))    
    el2 = []
    el3 = []
    for i in range(ncont):
        try:
            el2.append(containers[i][1])
        except IndexError:
            el2.append(0)
        try:
            el3.append(containers[i][2])
        except IndexError:
            el3.append(0)
            
    fig,ax = plt.subplots()
    
    inds = np.arange(1,ncont+1)
    ax.bar(inds,el1,color='royalblue')
    ax.bar(inds,el2,bottom=el1,color='orange')
    ax.bar(inds,el3,bottom=np.array(el1)+np.array(el2),color='forestgreen')
    
    ax.set_title(title)
    ax.set_xticks(np.arange(1,ncont+1,1))
    ax.set_yticks(np.arange(0,c+1,c/5))
    plt.tight_layout()
    if export == 1:
        plt.savefig(title+'.pdf',orientation='landscape')
    plt.show()

def check(bins):
    # function to check if the solution is valid (all the items are packed and
    # there is no capacity violation)
    tot = 0
    tru = []
    for el in bins:
        tot += sum(el)
        tru.append((sum(el)<=c))
    print(tot)
    if all(tru) == 1:
        print('all bins ok')
    else:
        print('at least 1 invalid bin')

instance = 'instances/Falkenauer_t60_00.txt'

(n,c,w) = importInstance(instance)
solution = bpp_greedy(instance)   
plotSolution(solution,"BPP - Solution greedy approach.")
check(solution)