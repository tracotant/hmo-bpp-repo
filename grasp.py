# HEURISTICS FOR MATHEMATICAL OPTIMIZAITON
# BIN PACKING PROBLEM SOLVER
# BY STEFANO CASARIN

import numpy as np
import matplotlib.pyplot as plt

def importInstance(name='default'):
    #import the given instance
    data = np.loadtxt(name,dtype=int)
    n = data[0] #n° of items
    c = data[1] #capacity of the bins
    w = data[2:] #weight of each item
    return (n,c,w)

def fillBin(unpacked,C):
    containers = []         # list of bins
    box = []                # an empty bin
    spaceleft = C       
    while len(unpacked):    # until there are unpacked items
        if len(unpacked) >=ngrasp: # if there are "ngrasp" or more unpacked items
            item = np.random.randint(ngrasp) #pick one at random
        else:
            m = len(unpacked) -1            
            if m > 0:           # if there are 0<n<ngrasp items
                item = np.random.randint(m) # pick one of those random
            else:
                item = 0        # else take the last
        weight = unpacked[item] 
        if weight < spaceleft:  # if it fits
            box.append(weight)  # put it in the box
            spaceleft -= weight # calculate the remaining free space
            unpacked.pop(item)  # remove the item from the unpacked list
        else:                   # if the bin is full
            box.sort(reverse=False)   # sort the items in the box
            containers.append(box) # add the bin to the list of full bins
            box = []            # new bin
            spaceleft = C
    containers.append(box)
    return containers

def bpp_grasp(instance,runs):
    (n,c,w) = importInstance(instance)
    w.sort()
    unpacked = list(w)          # list of items not packed yet
    solutions = []
    for i in range(runs):       # perform several runs
        containers = fillBin(unpacked,c)
        containers.sort(reverse=False) # sort containers 
        if not containers in solutions: # if solution already exists pass
            solutions.append(containers)
        unpacked = list(w)
    return solutions

def bestSol(solutions):
    lensol = [len(solutions[i]) for i in range(len(solutions))] #how many bins in each solution
    bestsol = solutions[np.argmin(lensol)] #the minimum amount of bins
    return bestsol
    
def plotSolution(containers,title,export=0):
    (n,c,w) = importInstance(instance)
    ncont = len(containers)
    el1 = tuple(containers[i][0] for i in range(ncont))    
    el2 = []
    el3 = []
    for i in range(ncont):
        try:
            el2.append(containers[i][1])
        except IndexError:
            el2.append(0)
        try:
            el3.append(containers[i][2])
        except IndexError:
            el3.append(0)
            
    fig,ax = plt.subplots()
    
    inds = np.arange(1,ncont+1)
    ax.bar(inds,el1,color='royalblue')
    ax.bar(inds,el2,bottom=el1,color='orange')
    ax.bar(inds,el3,bottom=np.array(el1)+np.array(el2),color='forestgreen')
    
    ax.set_title(title)
    ax.set_xticks(np.arange(1,ncont+1,1))
    ax.set_yticks(np.arange(0,c+1,c/5))
    plt.tight_layout()
    if export == 1:
        plt.savefig(title+'.pdf',orientation='landscape')
    plt.show()
    
def printsol(solutions):
    lensol = [len(solutions[i]) for i in range(len(solutions))]
    bestsolno = min(lensol)
    nsols = lensol.count(bestsolno)
    tot = 0
    for el in best:
        tot += sum(el)
    if sum(w) == tot:
        success = "ALL ITEMS PACKED SUCCESSFULLY"
    else:
        "Error: some items are unpacked."
    print("{0:^s}".format("########### BPP -- Grasp solver ###########"))
    print("Solver status: ",success)
    print("{0:<s} {1:>d}".format("Min. number of bins:",bestsolno))
    print("{0:<s} {1:>d}".format("Number of solutions found:",nsols))
    return nsols

instance = 'instances/Falkenauer_t60_00.txt'
(n,c,w) = importInstance(instance)

runs = 100
ngrasp = int(len(w)/2) # first elements from which pick a random item
solutions = bpp_grasp(instance,runs) 
best = bestSol(solutions)  
plotSolution(best,"BPP - Solution grasp approach.")      # plot best solution
printsol(solutions)     # print
